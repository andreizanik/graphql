import React from 'react';
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import Form from './components/form/form.js';

const client = new ApolloClient({
  uri: "http://localhost:4000/graphql"
});

const App = () => {
  return (
    <ApolloProvider client={client}>
      <div className="container">
        <div className="row mt-3 mb-3">
          <Form />
        </div>
      </div>
    </ApolloProvider>
  );
};

export default App;
