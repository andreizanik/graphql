import React, { Component } from 'react';
import moment from 'moment';

class Timer extends Component {
  state = {
    currentTime: Date.now(),
  };

  componentDidMount() {
    this.timer = setInterval(
      () => this.setState({ currentTime: Date.now() }),
      100,
    );
  };

  componentWillUnmount() {
    clearInterval(this.timer)
  };

  render() {
    const { currentTime } = this.state;
    const { data } = this.props;

    const differenceTime =  moment(data).isAfter(currentTime)
      ? moment.duration(moment(data).diff(currentTime))
      : moment.duration(moment(currentTime).diff(data));

    return (
      <div className="col-4">
        <h4>Timer</h4>
        <h5>
          {differenceTime.years() ? `${differenceTime.years()} лет ` : null}
          {differenceTime.months() ? `${differenceTime.months()} месяцев ` : null}
          {differenceTime.days() ? `${differenceTime.days()} дней ` : null}
          {`${differenceTime.hours()}:${differenceTime.minutes()}:${differenceTime.seconds()}`}
        </h5>
      </div>
    );
  }
}

export default Timer;
