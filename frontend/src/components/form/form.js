import React, { useState } from 'react';
import { gql } from "apollo-boost";
import { Mutation, Query } from "react-apollo";
import DatePicker from 'react-datepicker';
import Timer from '../timer/timer.js';
import "react-datepicker/dist/react-datepicker.css";

const SET_DATE = gql`
  mutation setTime($username: String! $date: String!) {
    setTime(username: $username, date: $date)
  }
`;

const GET_DATE = gql`
  query getTime($username: String!) {
    getTime(username: $username)
  }
`;

const Form = () => {
  const [username, setUsername] = useState('');
  const [date, setDate] = useState(new Date());

  return (
    <Mutation mutation={SET_DATE}>
      {(setTime, { loading, data }) => {
        if (!data) {
          return (
            <div className="col-4">
              <form
                onSubmit={e => {
                  e.preventDefault();
                  setTime({ variables: { username, date } });
                }}
              >
                <div className="form-group">
                  <label htmlFor="username">Username</label>
                  <input
                    type="text"
                    className="form-control"
                    id="username"
                    value={username}
                    onChange={(event) => setUsername(event.target.value)}
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="username">Date</label>
                  <DatePicker
                    className="form-control"
                    selected={date}
                    onChange={(value) => setDate(value)}
                    showTimeSelect
                    timeFormat="HH:mm"
                    dateFormat="MMMM d, yyyy h:mm aa"
                  />
                </div>

                <button
                  type="submit"
                  className="btn btn-primary"
                  disabled={loading}>
                  {loading ? 'Loading' : 'Save'}
                </button>
              </form>
            </div>
          );
        }

        return (
          <Query query={GET_DATE} variables={{ username }}>
            {({ loading, error, data }) => {
              if (error) return "Error...";
              if (loading || !data) return "Loading...";

              return <Timer data={data.getTime} />;
            }}
          </Query>
        );
      }}
    </Mutation>
  );
};

export default Form;
