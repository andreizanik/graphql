const express = require('express');
const { ApolloServer, gql } = require('apollo-server-express');
const { set, get } = require('lodash');

const users = {};

const typeDefs = gql`
  scalar Date
   
  type Query {
    getTime(username: String): Date
  }
  
  type Mutation {
    setTime(username: String, date: String): Boolean
  }
`;

const resolvers = {
  Query: {
    getTime: (parent, args) => {
      return get(users, args.username)
    },
  },
  Mutation: {
    setTime: (parent, args) => {
      set(users, args.username, args.date);
      return true;
    }
  }
};

const server = new ApolloServer({
  typeDefs,
	resolvers,
});
const app = express();
server.applyMiddleware({ app });

app.listen({ port: 4000 }, () =>
	console.log(`Server ready at http://localhost:4000${server.graphqlPath}`)
);
